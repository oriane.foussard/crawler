# Crawler

## Description
Ce projet contient un crawler single-threaded. À partir d'une URL d'entrée unique (https://ensai.fr/). Notre programme trouve d'autres pages à explorer en analysant les balises de liens trouvées dans les documents précédemment explorés. Notre programme se termine lorsque le crawler arrive à 50 urls trouvées ou si il ne trouve plus de liens à explorer. Une fois terminé, notre programme écrira dans un fichier crawled_webpages.txt toutes les urls trouvées.


## Installation du projet

La première étape, installation du projet :

```
git clone https://gitlab.com/oriane.foussard/crawler
cd crawler
pip install -r requirements.txt
```
verfiez que vous posedez une version de `python3`.


## Lancer le crawler

```
python3 main.py

```

Le code du crawler se trouve dans le fichier `service/crawler.py`. Ce code est documenté et commenté, chaque choix d'implémentation est expliqué.

### Problèmes rencontrés
Lorsque le robots.txt n'existe pas ou qu'il met trop de temps à répondre (plus de 3s) alors nous n'ajoutons pas cet url à la base des url rencontrés.

## Lancer les l'enregistrement sur une base de données
Une des améliorations possible du crawler était de créer une base de données relationnelle pour stocker les pages web trouvées et leur âge. Cela permet de ne pas rêqueter trop souvent une même page web et ainsi respecter la politesse. Pour cela j'ai décidé de stocker l'url de la page comme clé primaire, la date de la dernière visite ainsi que le contenu de la page.

Après avoir fournis ses configuration de base de données dans le fichier `.env`, il est possible de voir ces améliorations en lançant le code : 

```
python3 bonus.py

```

## Authors

Oriane Foussard.
