import os
import psycopg2
from psycopg2.extras import RealDictCursor
from utils.singleton import Singleton
from dotenv import dotenv_values
from dotenv import load_dotenv


class DBConnection(metaclass=Singleton):
    """
    Classe technique pour n'ouvrir qu'une seule connexion à la Base de Données.
    Avec les informations fournis dans le fichier .env
    """
    def __init__(self):
        # Open the connection. 
        load_dotenv()
        self.__connection =psycopg2.connect(
            host= os.getenv('DB_HOST'),
            user= os.getenv('DB_USER'),
            password= os.getenv('DB_PASSWORD'),
            port= os.getenv('DB_PORT'),
            database= os.getenv('DB_NAME'),
            cursor_factory=RealDictCursor)

    @property
    def connection(self):
        """
        return the opened connection.

        :return: the opened connection.
        """
        return self.__connection
