import urllib.robotparser
import pandas as pd
import urllib.request
from bs4 import BeautifulSoup
import datetime
import socket
from dao.db_connection import DBConnection
from dao.urlDao import UrlDao
from metier.url import Url


class CrawlerSingleThreaded():
    """
    Permet de créer un crawler single-threaded à partir d'une URL d'entrée unique.
    """

    def __init__(self, urlStart: str, maxTrouve: int) -> None:
        self.urlStart = urlStart #représente l'url duquel on veut partir
        self.urlsTrouve = [urlStart]
        self.dataUrlVisite = pd.DataFrame(
            columns=['url_visite', 'date_creation', 'document'])
        self.maxTrouve = maxTrouve #represente le nombre maximum d'url à trouver

    def controleUrl(self, url: str) -> bool:
        """Cette méthode nous permet de savoir si un url peut et doit être visité.
        return : booléen représentant le droit de visiter une page"""
        socket.setdefaulttimeout(3) #pb avec le robots.txt de luisvuitton qui mettait trop de temps à répondre
        try:
            # on récupère le nom du domaine
            parsed_uri = urllib.request.urlparse(url)
            domainName = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
            # on crée l'objet capable de lire le robot.txt
            rp = urllib.robotparser.RobotFileParser()
            rp.set_url(domainName + 'robots.txt')
            rp.read()
            # on peut lire un url seulement si le robot.txt nous le permet
            return (rp.can_fetch("*", url))
        except:  # pb avec le type de url input, ex : de type None
            return False

    def visitePage(self, url: str) -> None:
        """Cette méthode permet de visiter une page html qui dont url est input."""
        try:
            # on récupère la date actuel pour pouvoir calculer l'âge de la page
            date = datetime.datetime.now()
            # ouvre la page
            docHtm = urllib.request.urlopen(url)
            # on lit le fichier html
            soup = BeautifulSoup(docHtm, 'html.parser')
            # obligatoire car probleme lors de la création de la base de donnée pour la colonne document
            content = docHtm.read().decode(docHtm.headers.get_content_charset())
            # ajoute url la date et le contenu du document à notre table pour pouvoir l'ajouter à notre base de données
            self.dataUrlVisite.loc[self.dataUrlVisite.shape[0]] = [
                url, date, content]
            #on crée une variables des Link trouvé à partir de l'url
            linkTrouveAPartirUrl = soup.find_all('a')
            #on ajoute seulement si la liste des url trouvé est inférieur à self.MaxTrouve
            while (len(self.urlsTrouve) < self.maxTrouve) and (len(linkTrouveAPartirUrl)>0) :
                link = linkTrouveAPartirUrl.pop(0).get('href')
                # si on a pas déjà visité cet url ou si cet url n'est pas dans la liste des url déjà trouvé alors on peut controler
                if (link not in self.dataUrlVisite['url_visite'].tolist()) and (link not in self.urlsTrouve) :
                # si on est authorisé à lire cet url alors
                    if self.controleUrl(link):
                        self.urlsTrouve.append(link)
                    else:  # si on ne peut visiter cette page, on pass
                        pass
        except:  # problèmeb avec urlopen : dans la liste à visiter présence de #header
            pass
        return

    def parcourir(self) -> None:
        """Cette méthode permet de visiter un nombre fixe d'url à partir de l'url fournis lors de l'instanciation de la classe"""
        # on décide de parcourir tant que l'on a pas trouvé self.maxTrouve url différentes (max visite est un nombre que l'on peut modifier)
        while (len(self.urlsTrouve) < self.maxTrouve) and (len(self.urlsTrouve) > 0):
            # on prend le premier lien rencontré
            urlAVisiter = self.urlsTrouve.pop(0)
            self.visitePage(urlAVisiter)
        return

    def toTxt(self) -> None:
        """Cette méthode permet de stocker dans un fichier .txt l'ensemble des url que notre crawler à trouvé"""
        with open("crawled_webpages.txt", "w") as file:
            for url in self.urlsTrouve:
                file.write(str(url) + " \n")
        return

    def toSql(self) -> None:
        """Cette méthode permet de stocker sous format de base de donnée SQL,stocker les pages web visitées ansi que leur age"""
        self.dataUrlVisite.apply(lambda ligne: UrlDao().add_url(
            Url(ligne['url_visite'], ligne['date_creation'], ligne['document'])), axis=1)
        return
