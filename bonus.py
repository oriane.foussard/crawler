from dao.urlDao import UrlDao
from service.crawler import CrawlerSingleThreaded

if __name__ == '__main__':
    UrlDao().create_urldoc_table() #attention crée la table si n'existe pas, et la supprime avant de la créer sinon
    crawlerST = CrawlerSingleThreaded(urlStart="https://ensai.fr/", maxTrouve=50)
    crawlerST.parcourir()
    crawlerST.toSql()
