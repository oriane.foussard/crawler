from metier.url import Url
from utils.singleton import Singleton
from dao.db_connection import DBConnection


class UrlDao(metaclass = Singleton):
    def url_existe(self, urlDoc : Url) -> bool :
        """Renvoie un booléen qui nous indique si l'url existe déjà."""
        with DBConnection().connection as connection:
            with connection.cursor() as cursor :
                cursor.execute(
                    "SELECT * FROM urldoc WHERE url_visite=%(url)s;", 
                    {"url": urlDoc.url}
                )
                res = cursor.fetchall()
        
        existe = False
        if res :
            for row in res :
                existe = True
        return existe
    
    def create_urldoc_table(self) -> None :
        """Crée la table urldoc pour pouvoir stocker les url"""
        with DBConnection().connection as connection:
            with connection.cursor() as cursor :
                cursor.execute( 'DROP TABLE IF EXISTS urldoc ; CREATE TABLE urldoc  (url_visite text, date_creation text,document text,PRIMARY KEY (url_visite));')
        return

    def add_url(self, urlDoc : Url) -> None :
        """Ajoute une url à la base de données"""
        if self.url_existe(urlDoc= urlDoc) :
            return 
        else : 
            with DBConnection().connection as connection:
                with connection.cursor() as cursor :
                    cursor.execute( "INSERT INTO urldoc (url_visite, date_creation, document) VALUES (%(url)s, %(date)s, %(document)s);",
                        {"url": urlDoc.url, "date": urlDoc.date, "document": urlDoc.document })
        return

